#define potmeter A3
#define testLED 13
#define readPIN 8

void setup() {
  // put your setup code here, to run once:
  pinMode(3, OUTPUT);
  pinMode(testLED, OUTPUT);
  pinMode(readPIN, OUTPUT);
  digitalWrite(readPIN, LOW);
  digitalWrite(testLED, LOW);
  Serial.begin(1200);
  delay(6000);
}

void loop() {
  
  unsigned int waarde = 60;
  
  int val[8];
  for (int i = 7; i >= 0; i--)
  {
    val[i] = bitRead(waarde, i);
    Serial.print(val[i]);
  }
  Serial.print("\n");

  digitalWrite(readPIN, 1);
  for (int j = 0; j < 8; j++) {
    digitalWrite(testLED, val[j]);
    delay(100);
  }
  digitalWrite(readPIN, LOW);
  memset(val, 0, sizeof(val));

  delay(1000);
}

void printBits(byte myByte) {
  for (byte mask = 0x80; mask; mask >>= 1) {
    if (mask  & myByte)
      Serial.print('1');
    else
      Serial.print('0');
  }
}

























/*


   #define potmeter A3

  void setup() {
  // put your setup code here, to run once:
  pinMode(potmeter, INPUT);
  Serial.begin(9600);
  }

  void loop() {
  unsigned int waarde = analogRead(potmeter);
  waarde = map(waarde, 0, 1023, 0, 255);

  digitalWrite(3, 0); //Startbit

  for (int i = 1; i < 9; i++)
  {
    digitalWrite(3, bitRead(waarde, i-1));
    Serial.print(bitRead(waarde, i-1));
    delay(100);
  }
  Serial.println();

  digitalWrite(3, 0); //Stopbit

  delay(1000);
  }

  void printBits(byte myByte) {
  for (byte mask = 0x80; mask; mask >>= 1) {
    if (mask  & myByte)
      Serial.print('1');
    else
      Serial.print('0');
  }
  }
 * */
